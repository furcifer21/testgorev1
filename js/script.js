 document.addEventListener("DOMContentLoaded", function(event) {
 //календарь
	let today = new Date();
	let currentMonth = today.getMonth();
	let currentYear = today.getFullYear();
	let selectYear = document.getElementById("year");
	let selectMonth = document.getElementById("month");

	let months = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"];

	let monthAndYear = document.getElementById("monthAndYear");
	showCalendar(currentMonth, currentYear);


	function next(){
	  currentYear = (currentMonth === 11) ? currentYear + 1 : currentYear;
	  currentMonth = (currentMonth + 1) % 12;
	  showCalendar(currentMonth, currentYear);
	}

	function previous(){
	  currentYear = (currentMonth === 0) ? currentYear - 1 : currentYear;
	  currentMonth = (currentMonth === 0) ? 11 : currentMonth - 1;
	  showCalendar(currentMonth, currentYear);
	}
	
	document.getElementById("previous").addEventListener('click', function() { previous() }, false);
	document.getElementById("next").addEventListener('click', function() { next() }, false);
	
	function showCalendar(month, year) {

	  //Берем текущую дату
	  const currDay = new Date(year, month);

	  //Находим дату начала недели
	  currDay.setDate(currDay.getDate() - currDay.getDay() + 1);

	  //Делаем заголовок дней недели
	  const tbl = document.getElementById("calendar-body"); 
	  var weekDays = ['Понедельник, ', 'Вторник, ', 'Среда, ', 'Четверг, ', 'Пятница, ', 'Суббота, ', 'Воскресенье, '  ];

	  // filing data about month and in the page via DOM.
	  monthAndYear.innerHTML = months[month] + " " + year;

	  tbl.innerHTML = '';

	  //Пока понедельник не выходит за пределы текущего месяца
	  // добавляем строки
	  do {
		const row = document.createElement("tr");
		for(let i = 0; i < 7; i++) {
		  const classes =
			currDay.getDate() === today.getDate() &&
			currDay.getFullYear() === today.getFullYear() &&
			currDay.getMonth() === today.getMonth()
			 ? ' class="today"' : '';

		  row.innerHTML += `<th${classes}>${weekDays.shift() || ''} ${currDay.getDate()}</th>`;
		  //Увеличиваем дату на единицу
		  currDay.setDate(currDay.getDate() + 1);
		}
		tbl.appendChild(row);
	  } while (currDay.getMonth() === month);
	}
//все остальное
	document.getElementById("add-btn").addEventListener("click", () => { 
		document.getElementById("add-btn").classList.add('active');
		document.getElementById("add-note").style.display = 'block';
	}, false);
	document.getElementById("close").addEventListener("click", () => { 
		document.getElementById("add-btn").classList.remove('active');
		document.getElementById("add-note").style.display = 'none';
	}, false);
	
	var close_note=document.getElementsByClassName('close');
	
	function closeAll(){
		document.getElementById("add-note__quick").style.display = 'none';
	}
	for (var i = 0 ; i < close_note.length; i++) {
	   close_note[i].addEventListener('click' , closeAll , false ) ; 
	}
	
	var selectedTd;
	var addNote;
	var table=document.getElementById("calendar");;
	
	table.onclick = function(event) {
	  var target = event.target; // где был клик?

	  if (target.tagName != 'TH') return; // не на TH? тогда не интересует

	  note(target); // подсветить TH
	  addNote__quick(target); //создать форму "добавить заметку"
	};

	function note(node) {
	  if (selectedTd) {
		selectedTd.classList.remove('note');
	  }
	   selectedTd = node;
	  selectedTd.classList.add('note');
	}
	function addNote__quick(){
		if (addNote) {
			addNote.parentNode.removeChild(addNote);
		  }
		addNote = document.createElement('form');
		addNote.id = "add-note__quick";
		
		addNote.innerHTML ='<i id="close2">&times;</i>'+
							'<input type="text"  class="field" placeholder="Событие">'+
							'<input type="text"  class="field" placeholder="День, месяц, год">'+
							'<input type="text"  class="field" placeholder="Участники">'+
							'<textarea class="field" placeholder="Описание"></textarea>'+
							'<button type="submit">Готово</button>'+
							'<button type="submit">Удалить</button>'
		selectedTd.appendChild(addNote);
		document.getElementById("close2").addEventListener("click", () => { 
			document.getElementById("add-note__quick").style.display = 'none';
		}, false); 
	}
	
});